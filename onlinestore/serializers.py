from requests import models
from .models import Item, User, Cart_Item, Cart
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'


class CartItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cart_Item
        fields = ('item_cart_id', 'cart',
                  'item', 'quantity', 'item_amount','update_stock')


class CartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cart
        fields = ('id', 'user', 'total_price')
