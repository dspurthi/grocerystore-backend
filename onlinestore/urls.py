from django import urls
from django.urls import path
# from django.contrib import admin

# from . import views
# from .models import Item
# from .serializers import ItemSerializer
from .views import items_list,item_details,users,user_details,cart_items,user_cart

urlpatterns = [
    path('items/',items_list),
    path('details/<int:pk>/',item_details),
    path('users/',users),
    path('user_detail/<int:pk>/',user_details),
    path('cart_items/',cart_items),
    path('cart/',user_cart)

]