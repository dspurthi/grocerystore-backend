from pickle import FALSE
from tkinter import CASCADE
from unicodedata import category
from django.contrib.postgres.fields import ArrayField
from django.db import models,migrations
from django.db.models import Count
from django.db.models import Sum

# Create your models here.

class Item(models.Model):
    item_id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=30)
    price = models.FloatField()
    weight = models.CharField(max_length=30)
    best_before = models.DateField()
    stock_availability = models.IntegerField()
    category = models.CharField(max_length=30)
    brand = models.CharField(max_length=30)


# Create User

class User(models.Model):
    user_id = models.BigAutoField(primary_key=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    mail_id = models.CharField(max_length=100,unique=True)
    address = models.CharField(max_length=500)
    phone = models.BigIntegerField()

class Cart(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    @property
    def total_price(self):
        cart_items = Cart_Item.objects.filter(cart= self.id)
        total = 0
        for cart_item in cart_items:
            item_amount = cart_item.quantity * cart_item.item.price
            total += item_amount
        return total

class Cart_Item(models.Model):
    item_cart_id = models.BigAutoField(primary_key=True)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    quantity = models.IntegerField(choices=list(zip(range(1, 10), range(1, 10))))


    def update_stock(self):
        update_item = Item.objects.get(item_id = self.item.item_id)
        print(update_item)
        update_item.stock_availability = update_item.stock_availability - self.quantity
        if update_item.stock_availability >= 0:
            update_item.save()
            return update_item.stock_availability
        
        else:
            return "stock not available"

    @property
    def item_amount(self):
        item = self.item_id
        return item.price * self.quantity

    

    # def save(self, *args, **kwargs):
    #     super(Cart_Item, self).save(*args, **kwargs)
    #     item = Item.objects.get(self.item)
    #     Item.stock_availablity = item.stock_availablity - self.quantity
    #     Item.save()
# Create Orders

# class Order(models.Model):
#     order_id = models.BigAutoField
    
#     total_amount = models.CharField(max_length=100)
#     order_date = models.CharField(max_length=100)
#     cart_id = models.ForeignKey(Cart_final,on_delete=models.CASCADE)



