from ast import Pass
from django.shortcuts import render
from django.http import HttpResponse
from .models import  Cart, Cart_Item, Item,User
from .serializers import ItemSerializer,UserSerializer,CartItemsSerializer,CartSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt


def index(request):
    return HttpResponse("Hello, world. You're at Poochi Online Grocery Store !!!!!!!!")

# GET and POST Items to Store

@csrf_exempt
def items_list(request):
    if request.method == 'GET':
        items = Item.objects.all()
        serializer = ItemSerializer(items, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ItemSerializer(data=data,many = True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201,safe=False)
        return JsonResponse(serializer.errors, status=400)

# GET,UPDATE AND DELETE ITEMS BY ID

@csrf_exempt
def item_details(request,pk):
    try:
        item = Item.objects.get(pk=pk)

    except Item.DoesNotExist:
        return HttpResponse(status = 404)

    if request.method == 'GET':
        serializer = ItemSerializer(item)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ItemSerializer(item,data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        Item.delete()
        return HttpResponse(status=204)

# GET ALL USERS OR POST USER DATA


@csrf_exempt

def users(request):
    if request.method == 'GET':
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data,many=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201,safe=False)
        return JsonResponse(serializer.errors, status=400)


# GET,UPDATE OR DELETE USER BY ID

@csrf_exempt
def user_details(request,pk):
    try:
        user = User.objects.get(pk=pk)

    except User.DoesNotExist:
        return HttpResponse(status = 404)

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = UserSerializer(user,data=data)
        if serializer.is_valid():
            serializer.save()
            # user.refresh_from_db()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        User.delete()
        return HttpResponse(status=204)


# Get or Post or Update or Delete Cart

@csrf_exempt

def cart_items(request):
    if request.method == 'GET':
        cart_Item = Cart_Item.objects.all()
        serializer = CartItemsSerializer(cart_Item, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CartItemsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201,safe=False)
        return JsonResponse(serializer.errors, status=400)

# Final cart get post update and delete

@csrf_exempt

def user_cart(request):
    if request.method == 'GET':
        final_cart = Cart.objects.all()
        serializer = CartSerializer(final_cart, many=True)
        print(serializer.data)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CartSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            print(serializer.data)
            return JsonResponse(serializer.data, status=201,safe=False)
        return JsonResponse(serializer.errors, status=400)


# GET POST UPDATE DELETE ORDER

# def order(request):
#     if request.method == 'GET':
#         orders = Order.objects.all()
#         serializer = OrderSerializer(orders, many=True)
#         return JsonResponse(serializer.data, safe=False)

#     elif request.method == 'POST':
#         data = JSONParser().parse(request)
#         serializer = OrderSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=201,safe=False)
#         return JsonResponse(serializer.errors, status=400)